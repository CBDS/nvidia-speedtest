# nvidia-speedtest

Speed test for nvidia card/cluster on the basis of the DeepSolaris datasets.

The speed test comes with a docker to run the script. To build this docker execute:

```sh
$ ./build_docker
```

To run the docker you first need to export the dataset_dir and dataset that you want to use.
The dataset_dir will be mounted in the docker under /dataset. To specify dataset, you need
to choose the csv file with the annotations that should be present in the /dataset directory.

```sh
$ export dataset_dir="/path/to/your/dataset"
$ export dataset="/dataset/annotations.csv"
$ ./run_speedtest.sh
```

After that, in a results csv with the times per epoch will be saved in the current directory, i.e.
the directory run_speedtest.sh was run from.