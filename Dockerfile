FROM tensorflow/tensorflow:latest-gpu
LABEL MAINTAINER docker@thinkpractice.nl
WORKDIR /nvidia_speedtest
COPY requirements.txt .
RUN apt update && apt install -y libgl1-mesa-dev && apt clean
RUN pip install --upgrade pip && pip install -r requirements.txt
VOLUME ["/dataset", "/nvidia_speedtest"]
CMD ["python", "./nvidia_speedtest.py"]