#!/bin/bash
docker run --rm -it --runtime=nvidia -v ${PWD}:/nvidia_speedtest -v ${dataset_dir}:/dataset thinkpractice/nvidia_speedtest:latest python /nvidia_speedtest/nvidia_speedtest.py -d $dataset -o /nvidia_speedtest $@ -e 10
