from tensorflow.keras.applications import VGG16
from tensorflow.keras.applications.vgg16 import preprocess_input
from tensorflow.keras.layers import Flatten, Dense
from tensorflow.keras.models import Model
from sklearn.model_selection import train_test_split
import tensorflow
import argparse
import time
import csv
import os
import pandas as pd
from datetime import datetime


class TimeHistory(tensorflow.keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.times = []

    def on_epoch_begin(self, batch, logs={}):
        self.epoch_time_start = time.time()

    def on_epoch_end(self, batch, logs={}):
        self.times.append(time.time() - self.epoch_time_start)


def get_dataset(filename, batch_size, delimiter):
    df = pd.read_csv(filename, sep=delimiter)
    df["filename"] = df["filename"].str.replace(
        "/images/Heerlen_HR_2018_200x200_positives_png", "/dataset")
    df["filename"] = df["filename"].str.replace(
        "/images/Heerlen_HR_2018_200x200_negatives_png", "/dataset")
    df["filename"] = df["filename"].str.replace(".png", ".tiff")
    df["label"] = df["label"].astype(str)
    train_df, test_df = train_test_split(
        df, test_size=0.20, stratify=df["label"])

    dataset_directory = os.path.dirname(filename)
    train_generator = tensorflow.keras.preprocessing.image.ImageDataGenerator(
        preprocessing_function=preprocess_input)
    train_data = train_generator.flow_from_dataframe(
        train_df, directory=dataset_directory, y_col="label", target_size=(200, 200), batch_size=batch_size)

    test_generator = tensorflow.keras.preprocessing.image.ImageDataGenerator(
        preprocessing_function=preprocess_input)
    test_data = test_generator.flow_from_dataframe(
        test_df, directory=dataset_directory, y_col="label", target_size=(200, 200), batch_size=batch_size)

    return train_data, len(train_data.filenames), test_data, len(test_data.filenames)


def create_vgg16_model(input_shape):
    base_model = VGG16(input_shape=input_shape,
                       include_top=False, weights="imagenet")
    x = Flatten()(base_model.output)
    predictions = Dense(2, activation="softmax")(x)
    model_name = "vgg16"
    return model_name, Model(base_model.input, predictions)


parser = argparse.ArgumentParser()
parser.add_argument("-d", "--dataset", required=True,
                    help="The dataset to use for the speedtest")
parser.add_argument("-o", "--output-directory", required=True,
                    help="The directory to store the results")
parser.add_argument("-e", "--epochs", default=5, type=int,
                    help="The maximum number of epochs to evaluate")
parser.add_argument("-b", "--batch-size", default=64, type=int,
                    help="The batch size to use.")
parser.add_argument("-s", "--delimiter", default=";",
                    help="The delimiter used in the annotations csv")
args = parser.parse_args()

time_callback = TimeHistory()

train_generator, number_of_training_examples, test_generator, number_of_test_examples = get_dataset(
    args.dataset, args.batch_size, args.delimiter)

image_shape = (200, 200, 3)
optimizer = tensorflow.keras.optimizers.Adam()
_, model = create_vgg16_model(input_shape=image_shape)
model.compile(optimizer, loss="categorical_crossentropy", metrics=["accuracy"])

model.fit(train_generator, steps_per_epoch=number_of_training_examples // args.batch_size, batch_size=args.batch_size,
          epochs=args.epochs, validation_data=test_generator, validation_steps=number_of_test_examples // args.batch_size,
          callbacks=[time_callback])


results_date = datetime.now().strftime("%Y%m%d-%H:%M:%S")
with open(os.path.join(args.output_directory, f"results_{results_date}.csv")) as csv_file:
    csv_writer = csv.DictWriter(csv_file, fieldnames=["epoch", "time"])
    csv_writer.writeheader()
    for i, duration in enumerate(time_callback.times):
        csv_writer.writerow({"epoch": i, "duration": duration})
